/*
 * Copyright (c) 2017 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef PV_CONFIG_H
#define PV_CONFIG_H
#include<stdbool.h>
#include "utils/list.h"
enum {
	BL_UBOOT_PLAIN = 0,
	BL_UBOOT_PVK,
	BL_GRUB
};

struct pantavisor_factory {
	char *autotok;
};

struct pantavisor_creds {
	char *host;
	int port;
	char *id;
	char *prn;
	char *secret;
	char *token;
};

struct pantavisor_storage {
	char *path;
	char *fstype;
	char *opts;
	char *mntpoint;
};

struct pantavisor_updater {
	int interval;
	int keep_factory;
	int network_timeout;
};

struct pantavisor_bootloader {
	int type;
	int mtd_only;
	char *mtd_path;
};

struct pantavisor_watchdog {
	int enabled;
	int timeout;
};

struct pantavisor_network {
	char *brdev;
};

struct pantavisor_config {
	char *name;
	char *logdir;
	int logmax;
	int loglevel;
	int logsize;
	struct pantavisor_bootloader bl;
	struct pantavisor_creds creds;
	struct pantavisor_factory factory;
	struct pantavisor_storage storage;
	struct pantavisor_updater updater;
	struct pantavisor_watchdog wdt;
	struct pantavisor_network net;
};

// Fill config struct after parsing on-initramfs factory config
int pv_config_from_file(char *path, struct pantavisor_config *config);
int ph_config_from_file(char *path, struct pantavisor_config *config);
int ph_config_to_file(struct pantavisor_config *config, char *path);

struct pv_logger_config {
	struct dl_list item_list;
	/*
	 * This is a null terminated list of key/value
	 * pairs for the log configuration.
	 * */
	const char ***pair; /*equiv to char *pair[][2]. key, val*/
};
struct pv_log_info {
	const char *logfile;
	char *name;
	struct dl_list next;
	void (*on_logger_closed)(struct pv_log_info*);
	off_t truncate_size;
	bool islxc;
	pid_t logger_pid;
	const char*(*pv_get_log_config_item)
		(struct pv_logger_config *config, const char *key);
};

static void pv_free_logger_config(struct pv_logger_config *item_config)
{
	int i = 0;

	if (!item_config)
		return;

	while (item_config->pair[i][0]) {
		if (item_config->pair[i][1])
			free((void*)item_config->pair[i][1]);
		free((void*)item_config->pair[i][0]);
		free((void*)item_config->pair[i]);
		i++;
	}
	/*
	 * We've a NULL terminated pair..
	 * */
	free((void*)item_config->pair[i]);
	free(item_config);
}

#endif
